@extends('layout.boiler')
@section('body')
<div class="main">
    <div class="header">
        <h1><span><a href="/" style="text-align: left"><span style="color: white"><i class="fas fa-arrow-circle-left"></i></span></a></span> Edit Student Information</h1>
    </div>
    <div class="section">
        <div class="section-form">
            @if (Session::has('success'))
                <span id="message"><i class="fas fa-check-circle"></i> {{ Session::get('success') }}</span>
            @endif
            @foreach ($data as $item)
            <form action="/edit/{{ $item->studentID }}" method="POST">
                @csrf
                <table>
                    <tbody>
                            <tr>
                                <td>
                                    <label for="">Student Name <span style="color: red">*</span></label></td>
                                <td>
                                    <input type="text" placeholder="First Name" name="fname" value="{{ $item->first_name }}">
                                    <input type="text" placeholder="Middle Name" name="mname" value="{{ $item->middle_name }}">
                                    <input type="text" placeholder="Last Name" name="lname" value="{{ $item->last_name }}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Course <span style="color: red">*</span></label> 
                                </td>
                                <td>
                                    <select name="course" id="">
                                        <option value="BSIT" {{ ($item->course == 'BSIT') ? 'selected' : '' }}>BSIT</option>
                                        <option value="BSCPE" {{ ($item->course == 'BSCPE') ? 'selected' : '' }}>BSCPE</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="">Year Level <span style="color: red">*</span></label></td>
                                <td>
                                    <select name="yr_lvl" id="">
                                        <option value="1st Year" {{ ($item->year_level == '1st Year') ? 'selected' : '' }}>1st Year</option>
                                        <option value="2nd Year" {{ ($item->year_level == '2nd Year') ? 'selected' : '' }}>2nd Year</option>
                                        <option value="3rd Year" {{ ($item->year_level == '3rd Year') ? 'selected' : '' }}>3rd Year</option>
                                        <option value="4th Year" {{ ($item->year_level == '4th Year') ? 'selected' : '' }}>4th Year</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Date of Brith <span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input type="date" name="dob" value="{{ $item->date_of_birth }}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Home Address <span style="color: red">*</span></label><br>
                                </td>
                                <td>
                                    <input type="text" placeholder="Blk #/Street, City, Province" name="address" size="70" value="{{ $item->home_address }}">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button type="submit"><span><i class="fas fa-save"></i></span> Update</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                @endforeach
        </div>
    </div>
</div>
@endsection