@extends('layout.boiler')
@section('body')
    <div class="main">
        <div class="header">
            <h1><span><a href="/" style="text-align: left"><span style="color: white"><i class="fas fa-arrow-circle-left"></i></span></a></span> Student Registration</h1>
        </div>
        <div class="section">
            <div class="section-form">
                @if (Session::has('success'))
                    <span id="message"><i class="fas fa-check-circle"></i> {{ Session::get('success') }}</span>
                @endif
                <form action="/add" method="POST">
                    @csrf
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <label for="">Student Name <span style="color: red">*</span></label></td>
                                <td><input type="text" placeholder="First Name" name="fname">
                                    <input type="text" placeholder="Middle Name" name="mname">
                                    <input type="text" placeholder="Last Name" name="lname"></td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Course <span style="color: red">*</span></label> 
                                </td>
                                <td>
                                    <select name="course" id="">
                                        <option value="BSIT">BSIT</option>
                                        <option value="BSCPE">BSCPE</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="">Year Level <span style="color: red">*</span></label></td>
                                <td>
                                    <select name="yr_lvl" id="">
                                        <option value="1st Year">1st Year</option>
                                        <option value="2nd Year">2nd Year</option>
                                        <option value="3rd Year">3rd Year</option>
                                        <option value="4th Year">4th Year</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Date of Brith <span style="color: red">*</span></label>
                                </td>
                                <td>
                                    <input type="date" name="dob">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="">Home Address <span style="color: red">*</span></label><br>
                                </td>
                                <td>
                                    <input type="text" placeholder="Blk #/Street, City, Province" name="address" size="70">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button type="submit"><span><i class="fas fa-save"></i></span> Save</button>
                                    <button type="reset"><span><i class="fas fa-window-close"></i></span> Cancel</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection