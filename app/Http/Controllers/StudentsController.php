<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Students;
use Session;

class StudentsController extends Controller
{
    public function dashboard(){
        $data = DB::table('students')->where('status','0')->get();
        return view('dashboard', ['data' => $data]);
    }

    public function add(){
        return view('add');
    }
    
    public function store(Request $request){
        $std = new Students;
        $data = DB::table('students')->get();

        $fname = trim(htmlentities(\strip_tags($request->fname)));
        $mname = trim(htmlentities(\strip_tags($request->mname)));
        $lname = trim(htmlentities(\strip_tags($request->lname)));
        $course = trim(htmlentities(\strip_tags($request->course)));
        $yr_lvl = trim(htmlentities(\strip_tags($request->yr_lvl)));
        $dob = trim(htmlentities(\strip_tags($request->dob)));
        $address = trim(htmlentities(\strip_tags($request->address)));

        if(count($data) == 0){
            $id = 20200001;
            $std->studentID = $id;
            $std->first_name = $fname;
            $std->middle_name = $mname;
            $std->last_name = $lname;
            $std->course = $course;
            $std->year_level = $yr_lvl;
            $std->date_of_birth = $dob;
            $std->home_address = $address;
            $std->status = 0;
            $std->save();
            Session::flash('success', "You've Successfully Added a Student!");
            return redirect('/add');
        } else {
            $id = $data[count($data)-1]->studentID;
            $std->studentID = $id + 1;
            $std->first_name = $fname;
            $std->middle_name = $mname;
            $std->last_name = $lname;
            $std->course = $course;
            $std->year_level = $yr_lvl;
            $std->date_of_birth = $dob;
            $std->home_address = $address;
            $std->status = 0;
            $std->save();
            Session::flash('success', "You've Successfully Added a Student!");
            return redirect('/add');
        }
    }

    public function delete($id){
        $std = new Students;
        $std::where('studentID',$id)->update(['status'=>1]);
        return redirect('/');
    }

    public function edit($id){
        $data = DB::table('students')->where('studentID',$id)->get();
        return view('edit', ['data' => $data]);
    }

    public function update($id, Request $request){
        $std = new Students;

        $fname = trim(htmlentities(\strip_tags($request->fname)));
        $mname = trim(htmlentities(\strip_tags($request->mname)));
        $lname = trim(htmlentities(\strip_tags($request->lname)));
        $course = trim(htmlentities(\strip_tags($request->course)));
        $yr_lvl = trim(htmlentities(\strip_tags($request->yr_lvl)));
        $dob = trim(htmlentities(\strip_tags($request->dob)));
        $address = trim(htmlentities(\strip_tags($request->address)));

        $std::where('studentID', $id)->update(['first_name' => $fname, 'middle_name' => $mname, 'last_name' => $lname, 'course' => $course, 'year_level' => $yr_lvl, 'date_of_birth' => $dob, 'home_address' => $address]);


        Session::flash('success', "Successfully Updated!");
        return redirect('/edit/'.$id);
    }
}
